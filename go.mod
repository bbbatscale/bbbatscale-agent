module gitlab.com/bbbatscale/bbbatscale-agent

go 1.16

require (
	bbbatscale.de/recman v0.0.0-20211226182738-46f7dbab9edb
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/go-redis/redis/v8 v8.11.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/magiconair/properties v1.8.5
	go.uber.org/zap v1.19.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/tools v0.1.9 // indirect
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
