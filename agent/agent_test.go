package agent

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestIntervalAgent_Run(t *testing.T) {

	i, err := NewIntervalAgent(
		WithInterval(50*time.Millisecond),
		WithStartDelay(50*time.Millisecond),
	)
	if err != nil {
		t.Fatal(err)
	}

	type execution struct {
		interval time.Duration
		err      error
	}

	testAgent := struct {
		*IntervalAgent
		counter    int
		executions []execution
	}{
		IntervalAgent: i,
		counter:       0,
		executions: []execution{
			{0, nil},
			{200 * time.Millisecond, nil},
			{0, errors.New("simulated error")},
			{0, nil},
		},
	}

	type result struct {
		counter   int
		timestamp time.Time
	}

	resultC := make(chan result)
	testAgent.routine = func(ctx context.Context) (time.Duration, error) {
		defer func() {
			testAgent.counter++
		}()

		resultC <- result{
			counter:   testAgent.counter,
			timestamp: time.Now(),
		}

		e := testAgent.executions[testAgent.counter]
		return e.interval, e.err
	}

	ctx, cancelFunc := context.WithCancel(context.Background())
	go testAgent.Run(ctx)

	var results []result
	for r := range resultC {
		results = append(results, r)
		if len(results) >= len(testAgent.executions) {
			break
		}
	}
	cancelFunc()

	for i := 0; i < len(results)-1; i++ {

		var expected time.Duration
		executionInterval := testAgent.executions[i].interval
		executionErr := testAgent.executions[i].err
		if executionInterval > time.Duration(0) {
			expected = executionInterval
		} else if executionErr != nil {
			expected = testAgent.retryInterval.Backoff(0)
		} else {
			expected = testAgent.interval
		}

		actual := results[i+1].timestamp.Sub(results[i].timestamp)

		fmt.Println(expected, actual)

		if expected-2*time.Millisecond > actual || expected+2*time.Millisecond < actual {
			t.Errorf("with margin of 2ms expected: %s, actual %s", expected, actual)
		}
	}

}

type statsInterfacer struct {
	collected []*MeetingStats
	synced    ServerStats
}

func (s *statsInterfacer) SyncServerStats(ctx context.Context, stats ServerStats) (time.Duration, error) {
	s.synced = stats
	return 0, nil
}

func (s *statsInterfacer) GetMeetingStats(ctx context.Context) ([]*MeetingStats, error) {
	return s.collected, nil
}

func TestSyncStats(t *testing.T) {
	validOrigin := "BBB@Scale"

	interfacer := statsInterfacer{}
	interfacer.collected = []*MeetingStats{
		{
			IsBreakout: false,
			Origin:     validOrigin,
		},
		{
			IsBreakout: true,
			Origin:     validOrigin,
		},
		{
			IsBreakout: false,
			Origin:     "OriginElsewhere",
		},
	}

	dns := "host"
	agent, err := NewStatsAgent(&interfacer, &interfacer, dns)
	if err != nil {
		t.Fatal(err)
	}

	_, err = agent.routine(context.Background())
	if err != nil {
		t.Errorf("failed to execute routine of stats agent")
	}
	actual := interfacer.synced

	expected := ServerStats{
		MeetingStats: []*MeetingStats{
			{
				IsBreakout: false,
				Origin:     validOrigin,
			},
		},
		DNS: dns,
	}

	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("\nexpected:%+v\nactual:%+v", expected, actual)
	}
}
