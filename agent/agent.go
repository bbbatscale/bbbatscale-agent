package agent

import (
	"context"
	"gitlab.com/bbbatscale/bbbatscale-agent/internal/backoff"
	"go.uber.org/zap"
	grpcbackoff "google.golang.org/grpc/backoff"
	"time"
)

// IntervalAgent provides base to run agent functionality periodically
type IntervalAgent struct {
	logger *zap.Logger

	interval   time.Duration
	startDelay time.Duration

	retryInterval backoff.Strategy
	retries       int

	routine routine
}

// NewIntervalAgent instantiates a new IntervalAgent with default configuration. Configuration can be changed with agentOptions
func NewIntervalAgent(options ...IntervalAgentOption) (*IntervalAgent, error) {

	logger, err := zap.NewProduction()
	if err != nil {
		return nil, err
	}

	agent := &IntervalAgent{
		logger:        logger,
		interval:      5 * time.Second,
		startDelay:    5 * time.Second,
		retryInterval: &backoff.Exponential{Config: grpcbackoff.DefaultConfig},
		retries:       0,
		routine:       nil,
	}

	for _, option := range options {
		option(agent)
	}

	return agent, err
}

type routine func(ctx context.Context) (time.Duration, error)

func (a *IntervalAgent) setRoutine(r routine) {
	a.routine = r
}

func (a *IntervalAgent) resetRetries() {
	a.retries = 0
}

// IntervalAgentOption may be passed to NewIntervalAgent to configure IntervalAgent
type IntervalAgentOption func(agent *IntervalAgent)

// WithInterval returns IntervalAgentOption that sets the regular interval of IntervalAgent to provided duration
func WithInterval(d time.Duration) IntervalAgentOption {
	return func(agent *IntervalAgent) {
		agent.interval = d
	}
}

// WithRetryInterval returns IntervalAgentOption that sets the retry interval of IntervalAgent to provided duration.
// The retry interval is used in case of errors in the routine
func WithRetryInterval(s backoff.Strategy) IntervalAgentOption {
	return func(agent *IntervalAgent) {
		agent.retryInterval = s
	}
}

// WithLogger returns IntervalAgentOption that sets the internal logger of IntervalAgent
func WithLogger(logger *zap.Logger) IntervalAgentOption {
	return func(agent *IntervalAgent) {
		agent.logger = logger
	}
}

// WithStartDelay returns IntervalAgentOption that sets internal grace period of IntervalAgent to provided duration.
// The grace period delays the initial execution of the internal routine
func WithStartDelay(d time.Duration) IntervalAgentOption {
	return func(agent *IntervalAgent) {
		agent.startDelay = d
	}
}

// Run starts the periodic execution of the internal routine. This call is blocking due to internal loop, you might want to start it as a goroutine.
func (a IntervalAgent) Run(ctx context.Context) {

	if a.routine == nil {
		a.logger.Error("no routine provided to use")
		return
	}

	logger := a.logger.Sugar()
	timeC := make(chan time.Time, 1)
	signalTime := func() { timeC <- time.Now() }
	time.AfterFunc(a.startDelay, signalTime)

	for {
		select {
		case <-timeC:

			interval, err := a.routine(ctx)

			if err != nil {
				logger.Warnw("encountered error retrying", zap.Error(err))
				time.AfterFunc(a.retryInterval.Backoff(a.retries), signalTime)
				a.retries++
				continue
			}

			if interval > 0 {
				time.AfterFunc(interval, signalTime)
			} else {
				time.AfterFunc(a.interval, signalTime)
			}

			a.resetRetries()

		case <-ctx.Done():
			logger.Info("stop working")
			return
		}
	}
}
