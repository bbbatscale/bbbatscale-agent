package agent

import (
	"context"
	"time"
)

// Registerer registers information of the instance
type Registerer interface {
	Register(ctx context.Context, instance Instance) (time.Duration, error)
}

// RegistrationAgent is an agent that registers the server to BBB@Scale
type RegistrationAgent struct {
	*IntervalAgent
	registerer Registerer
}

// NewRegistrationAgent instantiates a new RegistrationAgent
func NewRegistrationAgent(i Instance, r Registerer, options ...IntervalAgentOption) (*RegistrationAgent, error) {
	agent, err := NewIntervalAgent(options...)
	if err != nil {
		return nil, err
	}

	ra := &RegistrationAgent{
		IntervalAgent: agent,
		registerer:    r,
	}

	agent.setRoutine(func(ctx context.Context) (time.Duration, error) {
		return ra.register(ctx, i)
	})

	return ra, err
}

// register starts a go routine that transfers instance information in intervals
func (a *RegistrationAgent) register(ctx context.Context, instance Instance) (time.Duration, error) {
	interval, err := a.registerer.Register(ctx, instance)
	return interval, err
}
