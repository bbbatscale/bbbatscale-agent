package agent

import (
	"errors"
	"fmt"
	"github.com/coreos/go-systemd/util"
	"github.com/magiconair/properties"
	"go.uber.org/zap"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

// Secreter is an interface that provides a secret
type Secreter interface {
	Secret() ([]byte, error)
}

// StaticSecret wrap string type to implement the Secreter interface
type StaticSecret string

// Secret provides the secret
func (s StaticSecret) Secret() ([]byte, error) {
	return []byte(s), nil
}

// Instance encapsulates information of the node
type Instance struct {
	Tenant             string
	SchedulingStrategy string
	Fqdn               string
	MachineID          string
	Properties         *PropertiesBBB
}

// NewInstance creates a new instance of Instance
func NewInstance(schedulingStrategy, tenant string, properties *PropertiesBBB) (*Instance, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	fqdn, err := net.LookupCNAME(hostname)
	if err != nil {
		// We could still try a reverse lookup starting from the IP
		// addresses. For now, what we have is probably good enough.
		return nil, err
	}
	// Omit trailing root zone dot as BBB@Scale chokes on it.
	// https://gitlab.com/bbbatscale/bbbatscale/-/issues/145
	fqdn = strings.TrimSuffix(fqdn, ".")

	machineID, err := util.GetMachineID()
	if err != nil {
		log.Fatalf("GetMachineID: %v", err)
	}
	return &Instance{
		Fqdn:               fqdn,
		MachineID:          machineID,
		Properties:         properties,
		SchedulingStrategy: schedulingStrategy,
		Tenant:             tenant,
	}, nil
}

// PropertiesBBB encapsulates BBB specific information
type PropertiesBBB struct {
	logger         *zap.SugaredLogger
	PropertiesFile string

	propertiesMu       sync.Mutex // protects the fields below
	properties         *properties.Properties
	propertiesLastStat os.FileInfo
}

// NewPropertiesBBB instantiates a new instance of PropertiesBBB
func NewPropertiesBBB(logger *zap.Logger, propertiesFiles []string) (*PropertiesBBB, error) {

	var propertiesFile string
	for _, file := range propertiesFiles {
		if file == "" {
			continue
		}
		_, err := os.Stat(file)
		if err == nil {
			propertiesFile = file
			break
		}
		if !errors.Is(err, os.ErrNotExist) {
			return nil, err
		}

	}

	return &PropertiesBBB{
		PropertiesFile: propertiesFile,
		logger:         logger.Sugar(),
	}, nil
}

// LoadSharedSecret reads properties file and extracts secret.
// returns secret as string and indicator if file has been updated since last read.
func (p *PropertiesBBB) LoadSharedSecret() (ss string, updated bool, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("loadSharedSecret: %w", err)
		}
	}()

	fi, err := os.Stat(p.PropertiesFile)
	if err != nil {
		return "", false, err
	}

	defer p.propertiesMu.Unlock()
	p.propertiesMu.Lock()

	// Short-circuit the file read if mtime==old_mtime and size==size.
	// Maybe check Inode, too?
	if p.propertiesLastStat == nil ||
		!fi.ModTime().Equal(p.propertiesLastStat.ModTime()) ||
		fi.Size() != p.propertiesLastStat.Size() {
		props, err := properties.LoadFile(p.PropertiesFile, properties.UTF8)
		if err != nil {
			p.logger.Warnw("load bbb-web properties file",
				"error", err,
				"file", p.PropertiesFile,
				"encoding", properties.UTF8,
			)
			return "", false, err
		}
		p.properties = props
		p.propertiesLastStat = fi
		updated = true
	}
	bbbSecret, ok := p.properties.Get("securitySalt")
	if !ok {
		err = fmt.Errorf("no securitySalt key in properties file (file: %s)",
			p.PropertiesFile)
		p.logger.Warnw("read securitySalt from bbb-web properties file",
			"error", err,
			"file", p.PropertiesFile,
			"encoding", properties.UTF8,
		)
		return "", false, err
	}

	return bbbSecret, updated, nil
}

// Secret wraps the LoadSharedSecret to provide the secret from properties file
func (p *PropertiesBBB) Secret() ([]byte, error) {
	secret, _, err := p.LoadSharedSecret()
	return []byte(secret), err
}
