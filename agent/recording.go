package agent

import (
	"bbbatscale.de/recman/recpb"
	"context"
	"errors"
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"gitlab.com/bbbatscale/bbbatscale-agent/recordings"
	"go.uber.org/zap"
	"golang.org/x/sync/singleflight"
	"google.golang.org/grpc"
	"path/filepath"
	"strings"
)

// RecordingDoneNotifier is the interface that defines methods to notify BBB@Scale regarding finished recordings
type RecordingDoneNotifier interface {
	NotifyRecordingDone(ctx context.Context, recording, meetingID, playbackURL, token string) (err error)
	NotifyRecordingDoneWithMetadataHack(ctx context.Context, recording, meetingID, playbackURL, token string, mdBytes []byte) (err error)
}

// RecordingAgent is an agent that tracks, uploads and publish recordings.
type RecordingAgent struct {
	log            *zap.Logger
	doneNotifier   RecordingDoneNotifier
	recordingsConn *grpc.ClientConn

	singleflightUploads singleflight.Group

	machineID             string
	scaleRecordingsSecret string
	recordingsBaseDir     string
}

// NewRecordingAgent instantiates a new RecordingAgent
func NewRecordingAgent(logger *zap.Logger, doneNotifier RecordingDoneNotifier, conn *grpc.ClientConn, machineID string, scaleRecordingsSecret string, recordingsBaseDir string) *RecordingAgent {
	return &RecordingAgent{
		log:                   logger,
		doneNotifier:          doneNotifier,
		recordingsConn:        conn,
		singleflightUploads:   singleflight.Group{},
		machineID:             machineID,
		scaleRecordingsSecret: scaleRecordingsSecret,
		recordingsBaseDir:     recordingsBaseDir,
	}
}

// TrackRecordings starts the main functionality of the RegistrationAgent.
func (a *RecordingAgent) TrackRecordings(ctx context.Context) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("trackRecordings: %w", err)
		}
	}()
	if a.recordingsConn == nil {
		return errors.New("no client conn configured")
	}

	recClient := recpb.NewRecordingsClient(a.recordingsConn)
	readyC := make(chan *recpb.Recording, 1)

	go func() {
		for {
			select {
			case r := <-readyC:
				a.log.Info("trackRecordings: start upload",
					zap.String("recording", r.Name))
				go func() {
					log := a.log.With(zap.String("recording", r.Name))
					rnew, err := a.uploadRecording(ctx, r)
					if err != nil {
						// BUG(ls): queue for retry!
						log.Error("trackRecordings: upload failed",
							zap.Error(err))
						return
					}
					r = rnew
					log.Info("trackRecordings: upload complete")
					err = a.doneNotifier.NotifyRecordingDone(ctx,
						r.Name,
						r.ExternalId,
						r.PlaybackUrl,
						a.scaleRecordingsSecret,
					)
					if err != nil {
						log.Warn("trackRecordings: notify failed: try metadata hack",
							zap.Error(err))
						err = a.doneNotifier.NotifyRecordingDoneWithMetadataHack(ctx,
							r.Name,
							r.ExternalId,
							r.PlaybackUrl,
							a.scaleRecordingsSecret,
							r.Metadata,
						)
						if err != nil {
							// BUG(ls): again, queue for retry!
							log.Error("trackRecordings: notify failed",
								zap.Error(err))
							return
						}
					}
					log.Info("trackRecordings: notified doneNotifier",
						zap.String("recording_playback_url", r.PlaybackUrl))
				}()
			case <-ctx.Done():
				return
			}
		}
	}()

	tracker := recordings.NewTracker(recClient,
		recordings.WithOrigin(a.machineID),
		recordings.WithReadyForUploadChan(readyC),
		recordings.WithLogger(a.log),
	)

	return tracker.TrackRecordings(ctx)
}

func (a *RecordingAgent) uploadRecording(ctx context.Context, r *recpb.Recording) (rn *recpb.Recording, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("uploadRecording: %w (recording: %s)",
				err, r.Name)
		}
	}()
	recID := strings.TrimPrefix(r.Name, "recordings/")
	recDir := filepath.Join(a.recordingsBaseDir, recID)
	eface, err, _ := a.singleflightUploads.Do(r.Name, func() (interface{}, error) {
		uploader, err := recordings.Upload(ctx,
			recpb.NewUploadsClient(a.recordingsConn), r,
			recDir, recordings.WithUploadLogger(a.log),
		)
		if err != nil {
			return nil, err
		}
		err = uploader.Upload(ctx)
		if err != nil {
			return nil, err
		}

		ctx := ctxzap.ToContext(ctx, a.log)
		recClient := recpb.NewRecordingsClient(a.recordingsConn)
		rnew, err := recordings.WaitForRecordingState(ctx, recClient, r.Name,
			recpb.Recording_READY, recpb.Recording_BROKEN)
		return rnew, err

	})
	if err != nil {
		return nil, err
	}
	return eface.(*recpb.Recording), err
}
