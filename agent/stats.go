package agent

import (
	"context"
	"time"
)

// MeetingStatsCollector is the interface that defines methods to collect meeting stats from BBB
type MeetingStatsCollector interface {
	GetMeetingStats(ctx context.Context) ([]*MeetingStats, error)
}

// ServerStatsExporter is the interface that defines methods to sync server stats with BBB@Scale
type ServerStatsExporter interface {
	SyncServerStats(ctx context.Context, stats ServerStats) (time.Duration, error)
}

// MeetingStats encapsulates all information of a meeting in BBB that are relevant for BBB@Scale
type MeetingStats struct {
	MeetingID      string `json:"meetingID"`
	BBBMeetingID   string `json:"BBBMeetingID"`
	RoomsMeetingID string `json:"roomsMeetingID"`

	Name             string `json:"name"`
	IsBreakout       bool   `json:"isBreakout"`
	ParticipantCount int    `json:"participantCount"`
	VideoCount       int    `json:"videoCount"`
	AttendeePW       string `json:"attendeePW"`
	ModeratorPW      string `json:"moderatorPW"`
	Creator          string `json:"creator"`

	Origin string `json:"origin"`
}

// ServerStats encapsulates all MeetingStats of the BBB server and necessary information of the server.
type ServerStats struct {
	MeetingStats []*MeetingStats `json:"meetings"`
	DNS          string          `json:"dns"`
}

// NewStatsAgent instantiates a new StatsAgent
func NewStatsAgent(collector MeetingStatsCollector, exporter ServerStatsExporter, dns string, options ...IntervalAgentOption) (*StatsAgent, error) {

	a, err := NewIntervalAgent(options...)
	if err != nil {
		return nil, err
	}

	s := &StatsAgent{
		IntervalAgent: a,
		collector:     collector,
		exporter:      exporter,
	}

	a.setRoutine(func(ctx context.Context) (time.Duration, error) {
		return s.syncStats(ctx, dns)
	})

	return s, nil
}

// StatsAgent is an agent that periodically collects MeetingStats from BBB to publish them to BBB@Scale
type StatsAgent struct {
	*IntervalAgent
	collector MeetingStatsCollector
	exporter  ServerStatsExporter
}

const scaleOrigin = "BBB@Scale"

func (a *StatsAgent) syncStats(ctx context.Context, dns string) (time.Duration, error) {
	stats, err := a.collector.GetMeetingStats(ctx)
	if err != nil {
		return 0, err
	}

	stats = filter(stats, func(sync *MeetingStats) bool { return sync.Origin == scaleOrigin })
	stats = filter(stats, func(sync *MeetingStats) bool { return !sync.IsBreakout })

	interval, err := a.exporter.SyncServerStats(ctx, ServerStats{
		DNS:          dns,
		MeetingStats: stats,
	})
	if err != nil {
		return 0, err
	}

	return interval, nil
}

type filterPredicate func(sync *MeetingStats) bool

func filter(meetings []*MeetingStats, predicate filterPredicate) []*MeetingStats {
	var filtered []*MeetingStats
	for _, m := range meetings {
		if predicate(m) {
			filtered = append(filtered, m)
		}
	}
	return filtered
}
