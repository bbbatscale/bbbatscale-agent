package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/bbbatscale/bbbatscale-agent/adapters"
	"gitlab.com/bbbatscale/bbbatscale-agent/adapters/bbb"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/daemon"
	"gitlab.com/bbbatscale/bbbatscale-agent/adapters/bbbatscale"
	"go.uber.org/zap"
	"go.uber.org/zap/zapgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/grpclog"
)

var (
	// Scale Identification
	scaleTenant = flag.String("t", os.Getenv("BBBATSCALE_AGENT_TENANT"),
		"`tenant` to register node with")
	scaleSchedulingStrategy = flag.String("sc", os.Getenv("BBBATSCALE_AGENT_SCHEDULING_STRATEGY"),
		"`scheduling strategy` to register node with")

	// Scale HTTP
	scaleURL = flag.String("c", os.Getenv("BBBATSCALE_AGENT_REMOTE_URL"),
		"remote `url` to register node with")
	scaleRegSecret = flag.String("s", os.Getenv("BBBATSCALE_AGENT_REGISTRATION_SECRET"),
		"tenant-specific `secret` for authenticating registration "+
			"requests (hex-encoded)")

	// Recordings-specific configuration. Setting the address for the
	// recordings API enables recordings tracking.
	enableRecordingsAgent = flag.Bool("enableRecordingsAgent", os.Getenv("ENABLE_AGENT_RECORDINGS") == "true",
		"flag to enable recording agent ")
	scaleRecordingsSecret = flag.String("r", os.Getenv("BBBATSCALE_AGENT_RECORDINGS_SECRET"),
		"`secret` token required for notifying BBB@Scale about recordings")
	recServerAddr = flag.String("rec-addr", os.Getenv("BBBATSCALE_AGENT_RECORDINGS_ADDR"),
		"connect to recordings API at `host:port`")
	recCert = flag.String("rec-cert",
		getEnvDefault("BBBATSCALE_AGENT_RECORDINGS_CERT", "/etc/bbbatscale-agent/cert.pem"),
		"`path` to PEM-encoded client certificate for recordings API")
	recKey = flag.String("rec-key",
		getEnvDefault("BBBATSCALE_AGENT_RECORDINGS_KEY", "/etc/bbbatscale-agent/key.pem"),
		"`path` to private key matching certificate recordings API")
	recTrust = flag.String("rec-trust", os.Getenv("BBBATSCALE_AGENT_RECORDINGS_TRUST"),
		"`path` to custom trust anchors used to authenticate recordings server")

	// BigBlueButton configuration
	bbbBaseURL = flag.String("bbbBaseURL", os.Getenv("BBB_BASE_URL"),
		"base `url` to BBB")

	// AgentIntervals
	enableRegistrationAgent = flag.Bool("enableRegistrationAgent", os.Getenv("ENABLE_AGENT_REGISTRATION") == "true",
		"flag to enable registration agent ")
	registrationInterval = flag.Duration("registrationInterval", getDurationFromEnv("AGENT_REGISTRATION_INTERVAL"),
		"set interval for periodic registration calls")

	enableStatsAgent = flag.Bool("enableStatsAgent", os.Getenv("ENABLE_AGENT_STATS") == "true",
		"flag to enable stats agent ")
	statsInterval = flag.Duration("statsInterval", getDurationFromEnv("AGENT_STATS_INTERVAL"),
		"set interval for periodic stats calls")
)

var bbbWebPropertiesFiles = []string{
	os.Getenv("BBBATSCALE_AGENT_BBBWEB_PROPERTIES"),
	"/etc/bigbluebutton/bbb-web.properties",
	"/usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties",
}

const defaultRecordingsBaseDir = "/var/bigbluebutton/published/presentation"

func getDurationFromEnv(envKey string) time.Duration {
	envValue := os.Getenv(envKey)

	if envValue == "" {
		log.Fatalf("envValue for duration in %s is empty \n", envKey)
	}

	duration, err := time.ParseDuration(envValue)
	if err != nil {
		log.Fatalf("unable to parse duration in %s: %s", envKey, err)
	}

	return duration
}

func main() {
	flag.Parse()
	logger := setupLogger()
	sugarLog := logger.Sugar()

	propsBBB, err := agent.NewPropertiesBBB(logger, bbbWebPropertiesFiles)
	if err != nil {
		sugarLog.Fatal(zap.Error(err))
	}

	scaleSecreter, err := setupScaleSecret()
	if err != nil {
		sugarLog.Fatal(zap.Error(err))
	}

	instance, err := setupInstance(propsBBB)
	if err != nil {
		sugarLog.Fatal(zap.Error(err))
	}

	scaleHTTP, err := setupScaleHTTPClient(scaleSecreter, instance.Tenant)
	if err != nil {
		sugarLog.Fatal(zap.Error(err))
	}

	bbbClient, err := setupBBBClient(propsBBB)
	if err != nil {
		sugarLog.Fatal(zap.Error(err))
	}

	globalCtx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)

	if *enableRegistrationAgent {
		ra, err := setupRegistrationAgent(logger, instance, scaleHTTP)
		if err != nil {
			sugarLog.Fatal(zap.Error(err))
		}
		go ra.Run(globalCtx)
	}

	if *enableStatsAgent {
		sa, err := setupStatsAgent(logger, instance, bbbClient, scaleHTTP)
		if err != nil {
			sugarLog.Fatal(zap.Error(err))
		}

		go sa.Run(globalCtx)
	}

	if *enableRecordingsAgent {
		ra, err := setupRecordingAgent(logger, instance, scaleHTTP)
		if err != nil {
			sugarLog.Fatal(zap.Error(err))
		}

		go func() {
			err := ra.TrackRecordings(globalCtx)
			sugarLog.Fatalw("trackRecordings returned unexpectedly", zap.Error(err))
		}()
	}

	err = setupWatchdog()
	if err != nil {
		sugarLog.Fatal(zap.Error(err))
	}

	// keep main thread alive
	for range globalCtx.Done() {
		logger.Info("shutdown...")
		daemon.SdNotify(false, daemon.SdNotifyStopping)
		stop()
		return
	}
}

func setupLogger() *zap.Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		// Just tear it down already.
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	zap.RedirectStdLog(logger)
	grpclog.SetLoggerV2(zapgrpc.NewLogger(logger))
	return logger
}

func setupWatchdog() error {
	watchdogPeriod, err := daemon.SdWatchdogEnabled(false)
	if err != nil {
		return fmt.Errorf("setupWatchdog: %w", err)
	}

	daemon.SdNotify(false, daemon.SdNotifyReady)
	watchdog := (<-chan time.Time)(nil)
	if watchdogPeriod != 0 {
		watchdog = time.Tick(watchdogPeriod / 2)
	}

	go func() {
		for range watchdog {
			daemon.SdNotify(false, daemon.SdNotifyWatchdog)
		}
	}()

	return nil
}

func setupInstance(propertiesBBB *agent.PropertiesBBB) (i *agent.Instance, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("setupInstance: %w", err)
		}
	}()

	if *scaleSchedulingStrategy == "" {
		err = errors.New("scaleSchedulingStrategy not set")
	}
	if *scaleTenant == "" {
		err = errors.New("scaleTenant not set")
	}

	i, err = agent.NewInstance(*scaleSchedulingStrategy, *scaleTenant, propertiesBBB)

	return
}

func setupRegistrationAgent(logger *zap.Logger, instance *agent.Instance, registerer agent.Registerer) (a *agent.RegistrationAgent, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("setupRegistrationAgent: %w", err)
		}
	}()

	if *scaleURL == "" {
		err = errors.New("scaleURL not set")
	}
	if *registrationInterval == 0 {
		err = errors.New("registrationInterval not set")
	}

	a, err = agent.NewRegistrationAgent(
		*instance,
		registerer,
		agent.WithLogger(logger),
		agent.WithInterval(*registrationInterval),
	)

	return
}

func setupScaleHTTPClient(secreter agent.Secreter, tenant string) (s *bbbatscale.ScaleHTTP, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("setupScaleHTTPClient: %w", err)
		}
	}()

	if *scaleURL == "" {
		err = errors.New("scaleURL not set")
	}
	if *scaleRegSecret == "" {
		err = errors.New("scaleRegSecret not set")
	}

	httpClient := &http.Client{
		// Set a relaxed base timeout that can be tightened as required
		// through context deadlines.
		Timeout: 10 * time.Second,
	}

	s, err = bbbatscale.New(
		*scaleURL,
		secreter,
		tenant,
		adapters.WithHTTPClient(httpClient),
	)

	return
}

func setupRecordingAgent(logger *zap.Logger, instance *agent.Instance, notifier agent.RecordingDoneNotifier) (a *agent.RecordingAgent, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("setupRecordingAgent: %w", err)
		}
	}()

	if *recServerAddr == "" {
		err = errors.New("recServerAddr not set")
	}
	if *recCert == "" {
		err = errors.New("recCert not set")
	}
	if *recKey == "" {
		err = errors.New("recKey not set")
	}
	if *recTrust == "" {
		err = errors.New("recTrust not set")
	}
	if *scaleRecordingsSecret == "" {
		err = errors.New("scaleRecordingSecret not set")
	}

	recConn, err := newSecureClientConn(*recServerAddr, *recCert, *recKey, *recTrust)

	a = agent.NewRecordingAgent(
		logger,
		notifier,
		recConn,
		instance.MachineID,
		*scaleRecordingsSecret,
		defaultRecordingsBaseDir,
	)

	return
}

func setupBBBClient(bbbSecreter agent.Secreter) (c *bbb.Client, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("setupBBBClient: %w", err)
		}
	}()

	if *bbbBaseURL == "" {
		err = errors.New("bbbBaseURL not set")
	}

	c, err = bbb.New(
		*bbbBaseURL,
		bbbSecreter,
		adapters.WithHTTPClient(
			&http.Client{Timeout: 10 * time.Second, Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}},
		))

	return
}

func setupStatsAgent(logger *zap.Logger, instance *agent.Instance, collector agent.MeetingStatsCollector, exporter agent.ServerStatsExporter) (a *agent.StatsAgent, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("setupStatsAgent: %w", err)
		}
	}()

	if *statsInterval == 0 {
		err = fmt.Errorf("statsInterval not set")
	}

	a, err = agent.NewStatsAgent(
		collector,
		exporter,
		instance.Fqdn,
		agent.WithLogger(logger),
		agent.WithInterval(*statsInterval),
	)

	return
}

func setupScaleSecret() (s agent.Secreter, err error) {
	decodedRegSecret, err := hex.DecodeString(*scaleRegSecret)
	if err != nil {
		return nil, fmt.Errorf("setupScaleSecret: %w", err)
	}

	s = agent.StaticSecret(decodedRegSecret)

	return
}

func newSecureClientConn(addr, clientCert, clientKey, trust string) (*grpc.ClientConn, error) {

	tlsConf, err := makeTLSConfig(clientCert, clientKey, trust)
	if err != nil {
		return nil, err
	}
	conn, err := grpc.Dial(addr,
		grpc.WithTransportCredentials(credentials.NewTLS(tlsConf)))
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func makeTLSConfig(certFile, keyFile, caFile string) (tlsConf *tls.Config, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("makeTLSConfig(%q, %q, %q): %w",
				certFile, keyFile, caFile, err)
		}
	}()
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}

	var caCerts *x509.CertPool
	if caFile != "" {
		// custom trust anchors requested
		pemBytes, err := os.ReadFile(caFile)
		if err != nil {
			return nil, err
		}
		caCerts = x509.NewCertPool()
		if !caCerts.AppendCertsFromPEM(pemBytes) {
			return nil, fmt.Errorf("no trust anchors read from %q", caFile)
		}
	}

	tlsConf = &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCerts,
		MinVersion:   tls.VersionTLS12,
	}

	return tlsConf, nil
}

func getEnvDefault(key, def string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return def
}
