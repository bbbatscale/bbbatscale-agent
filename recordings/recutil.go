package recordings

import (
	"context"
	"fmt"
	"gitlab.com/bbbatscale/bbbatscale-agent/internal/backoff"
	"time"

	"bbbatscale.de/recman/recpb"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.uber.org/zap"
	grpcbackoff "google.golang.org/grpc/backoff"
)

// WaitForRecordingState blocks until the state returned from a GetRecording
// invocation for the named recording is in wantStates.
func WaitForRecordingState(
	ctx context.Context,
	c recpb.RecordingsClient,
	recording string,
	wantStates ...recpb.Recording_State,
) (r *recpb.Recording, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("WaitForRecordingState(%q, %v): %w",
				recording, wantStates, err)
		}
	}()

	ctxzap.Info(ctx, "wait until recording reaches one of the provided states",
		zap.String("recording", recording),
		zap.Any("states", wantStates),
	)

	getReq := &recpb.GetRecordingRequest{Name: recording}
	timer := time.NewTimer(retryPolicy.Backoff(0))
	defer func() {
		if !timer.Stop() {
			<-timer.C
		}
	}()

	for i := 1; ; i++ {
		r, err = c.GetRecording(ctx, getReq)
		ctxzap.Info(ctx, "GetRecording",
			zap.Error(err),
			zap.String("recording", recording),
			zap.Any("state", r.GetState()),
		)
		if err == nil && indexRecordingState(wantStates, r.State) >= 0 {
			ctxzap.Info(ctx, "condition satisfied",
				zap.String("recording", recording),
				zap.Any("state", r.GetState()),
			)
			return r, nil
		}
		select {
		case <-ctx.Done():
			ctxzap.Info(ctx, "context canceled",
				zap.String("recording", recording),
				zap.Error(ctx.Err()),
			)
			return nil, ctx.Err()
		case <-timer.C:
			d := retryPolicy.Backoff(i)
			ctxzap.Info(ctx, "timer expired",
				zap.String("recording", recording),
				zap.Duration("next_attempt_in", d),
			)
			timer.Reset(d)
		}
	}
}

func indexRecordingState(haystack []recpb.Recording_State, needle recpb.Recording_State) int {
	for i, q := range haystack {
		if q == needle {
			return i
		}
	}
	return -1
}

var retryPolicy = backoff.Exponential{Config: grpcbackoff.Config{
	BaseDelay:  1.0 * time.Second,
	Multiplier: 1.6,
	Jitter:     0.2,
	MaxDelay:   10 * time.Second,
}}
