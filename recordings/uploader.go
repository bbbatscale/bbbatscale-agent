package recordings

import (
	"archive/zip"
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"time"

	"bbbatscale.de/recman/recpb"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

// An Uploader is responsible for uploading a single recording to the
// configured remote service.
type Uploader struct {
	rpc     recpb.UploadsClient
	rec     *recpb.Recording
	upload  *recpb.Upload
	fromDir string
	log     *zap.Logger
}

// An UploadOption customized the behaviour of an Uploader.
type UploadOption func(*Uploader)

// WithUploadLogger returns an option configuring the Uploader to write logs to
// l.
func WithUploadLogger(l *zap.Logger) UploadOption {
	return func(u *Uploader) {
		u.log = l
	}
}

// Upload returns an Uploader configured for the specified recording and remote
// service.
func Upload(
	ctx context.Context,
	rpc recpb.UploadsClient,
	rec *recpb.Recording,
	fromDir string,
	opts ...UploadOption,
) (u *Uploader, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("Upload: %s: %w", rec.Name, err)
		}
	}()

	// start by looking whether we can resume
	resp, err := rpc.ListUploads(ctx, &recpb.ListUploadsRequest{Parent: rec.Name})
	if err != nil {
		return nil, err
	}

	var best *recpb.Upload
	for _, candidate := range resp.Uploads {
		if best == nil || candidate.Offset > best.Offset {
			best = candidate
		}
	}
	var created bool
	if best == nil {
		req := &recpb.CreateUploadRequest{
			Parent: rec.Name,
		}
		up, err := rpc.CreateUpload(ctx, req)
		if err != nil {
			return nil, err
		}
		best = up
		created = true
	}

	uploader := &Uploader{
		rpc:     rpc,
		rec:     rec,
		upload:  best,
		fromDir: fromDir,
		log:     zap.NewNop(),
	}

	for _, opt := range opts {
		opt(uploader)
	}

	if created {
		uploader.log.Info("created upload",
			zap.String("name", best.Name),
			zap.String("recording", rec.Name),
		)
	} else {
		uploader.log.Info("resuming upload",
			zap.String("name", best.Name),
			zap.Int64("offset", best.Offset),
			zap.String("recording", rec.Name),
		)
	}

	return uploader, nil
}

// CreateRecordingZip packages up the recording data from u.fromDir into a Zip
// archive or returns an error, if any. The resulting stream can be read from
// the returned PipeReader. Close it to release associated resources.
func (u *Uploader) createRecordingZip(ctx context.Context) (*io.PipeReader, error) {
	log := u.log.Sugar()

	files, err := listFiles(u.fromDir)
	if err != nil {
		return nil, err
	}

	pr, pw := io.Pipe()
	zw := zip.NewWriter(pw)

	writeFile := func(f file) error {
		fd, err := os.Open(filepath.Join(u.fromDir, f.name))
		if err != nil {
			return err
		}
		defer fd.Close()

		fh := &zip.FileHeader{
			Name: f.name,
			// At least for media files, we *really* want to have
			// the identity function for compression, as that's
			// going to make it easier to serve byte range requests
			// directly from the zip file.
			Method: zip.Store,
		}
		// TODO(ls): set method to zip.Deflate for xml/json/… files
		w, err := zw.CreateHeader(fh)
		if err != nil {
			return err
		}
		if _, err := io.Copy(w, fd); err != nil {
			return err
		}
		return nil
	}

	go func() {
		for _, f := range files {
			if err := writeFile(f); err != nil {
				log.Error("writeFile", err)
				pw.CloseWithError(err)
				return
			}
		}
		pw.CloseWithError(zw.Close())
	}()

	return pr, nil
}

// Upload runs the upload process for the recording the receiver has been
// configured for. It only returns after the recording has been successfully
// uploaded, the upload process has failed irrevocably or ctx has been
// canceled.
func (u *Uploader) Upload(ctx context.Context) error {
	const (
		// upper bound on the number of bytes transferred in a single
		// request
		bufferSize = 4 << 20
		// max number of buffers to read ahead (memory usage is thus
		// roughly bufferSize*inFlightBuffers)
		inFlightBuffers = 1
	)
	log := u.log.Sugar()
	pr, err := u.createRecordingZip(ctx)
	if err != nil {
		return err
	}

	bufc := make(chan []byte, inFlightBuffers)
	errc := make(chan error, 1)
	go func() {
		for {
			// NOTE: allocation of the buffer MUST happen within
			// the loop body as we send it off to another
			// goroutine.
			buf := make([]byte, bufferSize)
			n, err := pr.Read(buf)
			if n > 0 {
				bufc <- buf[:n]
			}
			if err != nil {
				if err == io.EOF {
					log.Info("pr.Read: EOF")
					close(bufc)
					pr.Close()
					return
				}
				log.Error("pr.Read", err)
				errc <- err
				pr.Close()
				return
			}
		}
	}()

	dgst := sha256.New()
	var pipeOffset int64
Outer:
	for {
		select {
		case <-ctx.Done():
			// we got canceled ☹
			pr.CloseWithError(ctx.Err())
		case buf, ok := <-bufc:
			if !ok {
				// channel closed, EOF
				log.Info("select: bufc closed")
				break Outer
			}

			// Discard stream prefix until we reach the expected
			// offset to resume a previously aborted upload.
			if pipeOffset < u.upload.Offset {
				if pipeOffset+int64(len(buf)) <= u.upload.Offset {
					dgst.Write(buf)
					pipeOffset += int64(len(buf))
					continue Outer
				}
				n := len(buf) - int(u.upload.Offset-pipeOffset)
				dgst.Write(buf[:n])
				pipeOffset += int64(len(buf[:n]))
				buf = buf[n:]
			}
			pipeOffset += int64(len(buf))

			req := &recpb.WriteRequest{
				Name:    u.upload.Name,
				Offset:  u.upload.Offset,
				Content: buf,
			}

			log.Debugw("write",
				"upload", req.Name,
				"len", len(req.Content),
				"offset", req.Offset,
				"pipeoff", pipeOffset,
			)

			for {
				resp, err := u.rpc.Write(ctx, req)
				if err == nil {
					dgst.Write(buf)
					u.upload.Offset = resp.NewOffset
					log.Debugw("wrote", "upload", req.Name,
						"new_offset", resp.NewOffset)
					break
				}
				log.Errorw("write", "err", err, "upload", req.Name)
				switch status.Code(err) {
				case codes.FailedPrecondition, codes.NotFound, codes.OutOfRange:
					// cannot recover
					pr.CloseWithError(err)
					return err
				}
				select {
				case <-ctx.Done():
					continue Outer
				default:
				}
				// XXX backoff exponentially
				time.Sleep(2 * time.Second)
			}

		case err := <-errc:
			// Unrecoverable error.
			return err
		}
	}

	u.upload.Sha256 = dgst.Sum(nil)
	u.upload.Done = true
	u.upload.Length = u.upload.Offset
	mask, err := fieldmaskpb.New(u.upload, "length", "done", "sha256")
	if err != nil {
		// Invalid mask for type. All paths above are constant so this
		// error ought to be statically impossible (or is the result of
		// a programmer error in which case panicking is fine).
		panic(err)
	}

	req := &recpb.UpdateUploadRequest{
		Upload:     u.upload,
		UpdateMask: mask,
	}

	for {
		resp, err := u.rpc.UpdateUpload(ctx, req)
		if err == nil {
			log.Info("rpc.Update: ok")
			u.upload = resp
			break
		}
		log.Error("rpc.Update", err)
		// as above, increase backoff time exponentially
		time.Sleep(2 * time.Second)
	}

	return nil
}

type file struct {
	name string
	info os.FileInfo
}

func listFiles(dir string) ([]file, error) {
	var files []file
	err := filepath.Walk(dir, func(path string, st os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		rel, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}
		if !st.Mode().IsRegular() {
			return nil
		}

		files = append(files, file{name: rel, info: st})
		return nil
	})
	if err != nil {
		files = nil
	}
	sort.Slice(files, func(i, j int) bool {
		return files[i].name < files[j].name
	})
	return files, err
}
