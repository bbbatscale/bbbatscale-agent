package recordings

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"time"

	"bbbatscale.de/recman/recpb"
	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

// Tracker takes responsibility for getting recording data off the local
// machine. It subscribes to recording events from BBB, advising a remote RPC
// service about a recording state until they can be uploaded to off-machine
// stable storage.
type Tracker struct {
	rpc    recpb.RecordingsClient
	rdb    *redis.Client
	readyC chan<- *recpb.Recording

	log     *zap.Logger
	origin  string
	baseDir string
}

// A TrackerOption can be passed to NewTracker to customize a Tracker's
// behaviour.
type TrackerOption func(t *Tracker)

// WithRedisOptions configures a Tracker to use the provided redis.Options.
// Use this option to talk to a Redis DB different from the default DB 0 on
// localhost:6379.
func WithRedisOptions(opts *redis.Options) TrackerOption {
	return func(t *Tracker) {
		t.rdb = redis.NewClient(opts)
	}
}

// WithLogger makes the Tracker write logs to the provided Logger.
func WithLogger(l *zap.Logger) TrackerOption {
	return func(t *Tracker) {
		t.log = l
	}
}

// WithOrigin configures the Tracker to use name as the origin for recordings
// coming from the local machine (default is whatever os.Hostname returns).
func WithOrigin(name string) TrackerOption {
	return func(t *Tracker) {
		t.origin = name
	}
}

// WithBaseDir configures the base directory where BBB stores "published"
// recordings (default /var/bigbluebutton/published/presentation).
func WithBaseDir(path string) TrackerOption {
	return func(t *Tracker) {
		t.baseDir = path
	}
}

// WithReadyForUploadChan configures the tracker to notify about recordings
// that completed BBB processing and are ready for uploading elsewhere.
func WithReadyForUploadChan(c chan<- *recpb.Recording) TrackerOption {
	return func(t *Tracker) {
		t.readyC = c
	}
}

// NewTracker returns a Tracker configured to use rpcClient to talk to the
// recordings service and with the provided TrackerOptions applied, if any.
func NewTracker(rpcClient recpb.RecordingsClient, opts ...TrackerOption) *Tracker {
	hostname, err := os.Hostname()
	if err != nil {
		// Extremely unlikely to happen and not worth changing
		// NewTracker's signature for to return the error.
		panic("NewTracker: os.Hostname: " + err.Error())
	}

	t := &Tracker{
		rpc: rpcClient,
		rdb: redis.NewClient(&redis.Options{
			Addr: ":6379",
			DB:   0, // irrelevant for PubSub
		}),
		log:     zap.NewNop(),
		origin:  hostname,
		baseDir: "/var/bigbluebutton/published/presentation",
	}

	for _, opt := range opts {
		opt(t)
	}

	return t
}

const bbbFromRAPChannel = "bigbluebutton:from-rap"

// TrackRecordings subscribes to the appropriate channel on BBB's Redis PubSub,
// pushing recording state to the configured recordings service until asked to
// stop (by canceling ctx).
// Note that TrackRecordings does its work synchronously, so you probably want
// to start it in a new goroutine.
func (t *Tracker) TrackRecordings(ctx context.Context) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("TrackRecordings: %w", err)
		}
	}()

	sub := t.rdb.Subscribe(ctx, bbbFromRAPChannel)

	deadline, ok := ctx.Deadline()
	if ok {
		// Unblock channel range below at deadline (the Redis client
		// doesn't react to a canceled context).
		time.AfterFunc(time.Until(deadline), func() {
			sub.Close()
		})
	}

	for m := range sub.Channel() {
		rap := new(rapMessage)
		if err := json.Unmarshal([]byte(m.Payload), rap); err != nil {
			t.log.Error("unmarshal rapMessage",
				zap.Error(err),
				zap.String("channel", m.Channel),
				zap.String("payload", m.Payload),
			)
			continue
		}
		if err := t.processMessage(ctx, rap); err != nil {
			t.log.Error("process rapMessage",
				zap.Error(err),
				zap.String("channel", m.Channel),
				zap.String("payload", m.Payload),
			)
			continue
		}
	}

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		return nil
	}
}

func (t *Tracker) processMessage(ctx context.Context, m *rapMessage) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("processMessage: %w", err)
		}
	}()

	q := inferState(m)
	if q == recpb.Recording_UNKNOWN {
		return fmt.Errorf("unknown event name: %s", m.Header.Name)
	}

	meetingID := m.Payload.MeetingID
	if meetingID == "" {
		return fmt.Errorf("event %s: no meeting_id", m.Header.Name)
	}

	r := &recpb.Recording{
		Name:   recordingName(meetingID),
		Origin: t.origin,
		State:  q,
	}

	logFields := []zap.Field{
		zap.String("recording", r.Name),
		zap.String("state", q.String()),
		zap.String("event", m.Header.Name),
	}

	fm, err := fieldmaskpb.New(r, "name", "origin", "state")
	if err != nil {
		panic(err)
	}
	if externalID := m.Payload.ExternalMeetingID; externalID != "" {
		r.ExternalId = externalID
		if err := fm.Append(r, "external_id"); err != nil {
			panic(err)
		}
		logFields = append(logFields, zap.String("external_id", externalID))
	}
	if meta := []byte(m.Payload.Metadata); len(meta) > 0 {
		r.Metadata = meta
		if err := fm.Append(r, "metadata"); err != nil {
			panic(err)
		}
		logFields = append(logFields, zap.ByteString("metadata", meta))
	}

	if q == recpb.Recording_ARCHIVING {
		// We most likely see this recording for the first time, so
		// start with Create.
		t.log.Info("process rapMessage: trying create", logFields...)
		err := t.createRecording(ctx, r)
		if err == nil {
			t.log.Info("process rapMessage: created recording", logFields...)
			return nil
		}
		if e := errors.Unwrap(err); status.Code(e) != codes.AlreadyExists {
			// BUG(ls): Unexpected error, drop on the floor for
			// now. Need to think about how and when to retry.
			t.log.Error("process rapMessage: createRecording",
				append(logFields, zap.Error(e))...)
			return err
		}
		t.log.Info("process rapMessage: create: exists, trying update",
			logFields...)
	}

	err = t.updateRecording(ctx, r, fm)
	if e := errors.Unwrap(err); status.Code(e) == codes.NotFound {
		t.log.Info("process rapMessage: update: recording not found, trying create",
			logFields...)
		return t.createRecording(ctx, r)
	}
	t.log.Info("process rapMessage: updateRecording",
		append(logFields, zap.Error(err))...)

	return err
}

func (t *Tracker) createRecording(ctx context.Context, r *recpb.Recording) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("createRecording: %w", err)
		}
	}()

	in := &recpb.CreateRecordingRequest{
		Recording: r,
	}

	_, err = t.rpc.CreateRecording(ctx, in)
	return err
}

func (t *Tracker) updateRecording(
	ctx context.Context,
	r *recpb.Recording,
	fm *fieldmaskpb.FieldMask,
) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("updateRecording: %w", err)
		}
	}()

	in := &recpb.UpdateRecordingRequest{
		Recording:  r,
		UpdateMask: fm,
	}

	rn, err := t.rpc.UpdateRecording(ctx, in)
	if err == nil && rn.State == recpb.Recording_PUBLISHED {
		t.log.Info("updateRecording: ready for uploading",
			zap.String("recording", rn.Name),
			zap.String("state", rn.State.String()),
		)
		if t.readyC == nil {
			t.log.Warn("updateRecording: no uploader registered,"+
				" dropping notification",
				zap.String("recording", rn.Name),
			)
			return err
		}
		t.readyC <- rn
		t.log.Info("updateRecording: signaled channel for upload",
			zap.String("recording", rn.Name),
		)
	}
	return err
}

func recordingName(meetingID string) string {
	return path.Join("recordings", meetingID)
}

type rapMessage struct {
	Header  rapMessageHeader  `json:"header"`
	Payload rapMessagePayload `json:"payload"`
}

type rapMessageHeader struct {
	Timestamp   json.Number `json:"timestamp"`
	Name        string      `json:"name"`
	CurrentTime json.Number `json:"current_time"`
	Version     string      `json:"version"`
}

type rapMessagePayload struct {
	RecordID          string          `json:"record_id"`
	MeetingID         string          `json:"meeting_id"`
	Success           bool            `json:"success"`
	StepTime          json.Number     `json:"step_time"`
	ExternalMeetingID string          `json:"external_meeting_id"`
	Workflow          string          `json:"workflow"`
	Playback          json.RawMessage `json:"playback"`
	Metadata          json.RawMessage `json:"metadata"`
	Download          json.RawMessage `json:"download"`
	RawSize           json.Number     `json:"raw_size"`
	StartTime         json.Number     `json:"start_time"`
	EndTime           json.Number     `json:"end_time"`
}

func inferState(m *rapMessage) (q recpb.Recording_State) {
	switch m.Header.Name {
	case "archive_started":
		q = recpb.Recording_ARCHIVING
	case "archive_ended":
		if m.Payload.Success {
			q = recpb.Recording_ARCHIVED
		} else {
			q = recpb.Recording_ARCHIVING_FAILED
		}
	case "sanity_started":
		q = recpb.Recording_SANITY_CHECKING
	case "sanity_ended":
		if m.Payload.Success {
			q = recpb.Recording_SANITY_CHECKED
		} else {
			q = recpb.Recording_SANITY_CHECKING_FAILED
		}
	case "process_started":
		q = recpb.Recording_PROCESSING
	case "process_ended":
		if m.Payload.Success {
			q = recpb.Recording_PROCESSED
		} else {
			q = recpb.Recording_PROCESSING_FAILED
		}
	case "publish_started":
		q = recpb.Recording_PUBLISHING
	case "publish_ended":
		if m.Payload.Success {
			q = recpb.Recording_PUBLISHING
		} else {
			q = recpb.Recording_PUBLISHING_FAILED
		}
	case "post_publish_started":
		q = recpb.Recording_PUBLISHING
	case "post_publish_ended":
		q = recpb.Recording_PUBLISHED
	default:
		q = recpb.Recording_UNKNOWN
	}

	return q
}
