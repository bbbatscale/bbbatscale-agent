package bbbatscale

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestRegisterNode(t *testing.T) {

	expectedInterval := 10 * time.Second
	const tenant = "brave-fermat"
	secret, _ := hex.DecodeString("c674afa21a64434b8db07ffedd8fdf7e")

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			herror(w, http.StatusMethodNotAllowed)
			return
		}
		if r.Header.Get("Content-Type") != "application/json" {
			herror(w, http.StatusUnsupportedMediaType)
			return
		}
		if r.Header.Get("X-Bbbatscale-Experimental-Api") != "iacceptmyfate" {
			herror(w, http.StatusBadRequest)
			return
		}
		sig := r.Header.Get("X-Request-Signature")
		tenant := r.Header.Get("X-Bbbatscale-Tenant")
		if sig == "" || tenant == "" {
			herror(w, http.StatusBadRequest)
			return
		}
		body, err := ioutil.ReadAll(http.MaxBytesReader(w, r.Body, 1<<20))
		if err != nil {
			herror(w, 500)
			return
		}
		ok, ts := verifyRequestSignature(secret, body, sig)
		if !ok {
			herror(w, http.StatusForbidden)
		}
		if time.Since(ts) > time.Minute {
			herror(w, http.StatusForbidden)
		}

		w.Header().Set("Content-Type", "application/json")

		json.NewEncoder(w).Encode(
			struct {
				Interval float64 `json:"interval"`
			}{
				expectedInterval.Seconds(),
			},
		)

	}))
	defer ts.Close()

	c, err := New(ts.URL, agent.StaticSecret(secret), tenant)
	if err != nil {
		t.Fatal(err)
	}

	s := &Server{
		Hostname:     "happy-babbage.example.com.",
		MachineID:    "5a8b040132b3472c854e8d962669c948",
		SharedSecret: "a7tfbVF_KRgI2GNkx16dG5iXVrVGeh_l_gPpfqovDnA",
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	actualInterval, err := c.RegisterServer(ctx, s)

	if actualInterval != expectedInterval {
		t.Errorf("interval expected: %d, actual %d", expectedInterval, actualInterval)
	}

	if err != nil {
		t.Error(err)
	}
}

func herror(w http.ResponseWriter, code int) {
	http.Error(w, http.StatusText(code), code)
}

func verifyRequestSignature(key, body []byte, header string) (bool, time.Time) {
	v1, t, err := disassembleRequestSignature(header)
	if err != nil {
		return false, time.Time{}
	}
	tag, err := hex.DecodeString(v1)
	if err != nil {
		return false, time.Time{}
	}

	var b bytes.Buffer
	fmt.Fprintf(&b, "%d.%s", t, body)

	mac := hmac.New(sha512.New, key)
	mac.Write(b.Bytes())
	expect := mac.Sum(nil)
	return hmac.Equal(tag, expect), time.Unix(t, 0)
}

func disassembleRequestSignature(s string) (v1 string, t int64, err error) {
	elems := strings.Split(s, ",")
	if len(elems) < 2 {
		return "", 0, fmt.Errorf(
			"invalid argument: need at least v1 and t parts: %q", s)
	}

	for _, e := range elems {
		kv := strings.SplitN(e, "=", 2)
		if len(kv) != 2 {
			return "", 0, fmt.Errorf(
				"invalid argument: expect k=v, got %q", e)
		}
		switch kv[0] {
		case "v1":
			if kv[1] == "" {
				return "", 0, errors.New(
					"invalid argument: missing value for v1")
			}
			v1 = kv[1]
		case "t":
			d, err := strconv.ParseInt(kv[1], 10, 64)
			if err != nil {
				return "", 0, fmt.Errorf(
					"invalid argument: t: %v", err)
			}
			if d <= 0 {
				return "", 0, fmt.Errorf(
					"invalid argument: t: %d", d)
			}
			t = d
		default:
			// skip keys we don't know how to handle
		}
	}

	if v1 == "" {
		return "", 0, errors.New("invalid argument: missing v1")
	}
	if t == 0 {
		return "", 0, errors.New("invalid argument: missing t")
	}

	return v1, t, nil
}

func TestMarshallServerStats(t *testing.T) {
	serverstats := agent.ServerStats{
		MeetingStats: nil,
		DNS:          "host.test",
	}

	testCases := []struct {
		meetingStats []*agent.MeetingStats
		expected     string
	}{
		{
			meetingStats: []*agent.MeetingStats{
				{
					MeetingID:        "1272df44-aea7-4890-ba86-087122ee1f5e",
					BBBMeetingID:     "65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943",
					RoomsMeetingID:   "1272df44-aea7-4890-ba86-087122ee1f5e",
					Name:             "Mcguirefort",
					IsBreakout:       true,
					ParticipantCount: 1,
					VideoCount:       0,
					AttendeePW:       "CWuiF3q9NhwXGCGXRLVmYteaCFYogcax",
					ModeratorPW:      "tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk",
					Creator:          "admin",
					Origin:           "BBB@Scale",
				},
			},
			expected: `{"meetings":[{"meetingID":"1272df44-aea7-4890-ba86-087122ee1f5e","BBBMeetingID":"65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943","roomsMeetingID":"1272df44-aea7-4890-ba86-087122ee1f5e","name":"Mcguirefort","isBreakout":true,"participantCount":1,"videoCount":0,"attendeePW":"CWuiF3q9NhwXGCGXRLVmYteaCFYogcax","moderatorPW":"tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk","creator":"admin","origin":"BBB@Scale"}],"dns":"host.test"}`,
		},
		{
			meetingStats: nil,
			expected:     `{"meetings":[],"dns":"host.test"}`,
		},
	}

	for i, c := range testCases {
		serverstats.MeetingStats = c.meetingStats
		actual, err := marshalStats(serverstats)
		if err != nil {
			t.Fatal(err)
		}
		if string(actual) != c.expected {
			t.Errorf("TestCase: %d\nexpected:%s\nactual:%s", i, c.expected, actual)
		}
	}
}
