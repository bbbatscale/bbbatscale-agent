package bbbatscale

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/bbbatscale/bbbatscale-agent/adapters"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"net/http"
	"path"
	"strings"
	"time"
)

// ScaleHTTP provides an interface to BBBatScale. It is safe for concurrent use
// from multiple goroutines.
type ScaleHTTP struct {
	*adapters.Client
	tenant string
}

// New instanciates a new ScaleHTTP
func New(baseURL string, secreter agent.Secreter, tenant string, option ...adapters.ClientOption) (*ScaleHTTP, error) {
	client, err := adapters.New(baseURL, secreter, option...)
	if err != nil {
		return nil, err
	}
	return &ScaleHTTP{Client: client, tenant: tenant}, nil
}

func setHeader(h http.Header, body []byte, tenant string, secret []byte) http.Header {
	h.Set("Content-Type", "application/json")
	h.Set("X-Bbbatscale-Experimental-Api", "iacceptmyfate")
	h.Set("X-Bbbatscale-Tenant", tenant)
	h.Set("X-Request-Signature", adapters.ComputeMACHeader(secret, time.Now(), body))

	return h
}

// Register transfers instance information to BBB@Scale
func (c *ScaleHTTP) Register(ctx context.Context, instance agent.Instance) (time.Duration, error) {
	secret, err := instance.Properties.Secret()
	if err != nil {
		return 0, err
	}

	return c.RegisterServer(ctx, &Server{
		Hostname:     instance.Fqdn,
		MachineID:    instance.MachineID,
		SharedSecret: string(secret),
	})
}

// Server describes a machine running BigBlueButton.
type Server struct {
	Hostname     string `json:"hostname"`
	MachineID    string `json:"machine_id"`
	SharedSecret string `json:"shared_secret"`
}

// RegisterServer registers the server described by s with BBBatScale. The
// server is associated with tenant, and key is used for signing the request.
func (c *ScaleHTTP) RegisterServer(ctx context.Context, s *Server) (time.Duration, error) {
	secret, err := c.Secreter.Secret()
	if err != nil {
		return 0, err
	}

	u := *c.BaseURL
	u.Path = path.Join(u.Path, "servers/registration")
	regURL := u.String()

	body, err := json.Marshal(s)
	if err != nil {
		return 0, fmt.Errorf("RegisterServer: %v", err)
	}
	req, err := http.NewRequestWithContext(ctx, "POST", regURL,
		bytes.NewReader(body))
	if err != nil {
		return 0, fmt.Errorf("RegisterServer: %v", err)
	}
	req.Header = setHeader(req.Header, body, c.tenant, secret)

	resp, err := c.HTTP.Do(req)
	if err != nil {
		return 0, fmt.Errorf("RegisterServer: %w", err)
	}
	defer adapters.DrainAndClose(resp.Body)

	if !adapters.StatusOK(resp) {
		return 0, fmt.Errorf("RegisterServer: response status %s (code %d)",
			resp.Status, resp.StatusCode)
	}
	var registrationResp struct {
		Interval float64 `json:"interval"`
	}
	err = json.NewDecoder(resp.Body).Decode(&registrationResp)
	if err != nil {
		return 0, fmt.Errorf("RegisterServer: unmarshal failed %w", err)
	}

	return time.Duration(registrationResp.Interval) * time.Second, err
}

// NotifyRecordingDone signals BBBatScale that the recording for the meeting
// referred to by meetingID is available at playbackURL.
func (c *ScaleHTTP) NotifyRecordingDone(ctx context.Context, recording, meetingID, playbackURL, token string) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("NotifyRecordingDone: %s: %w", meetingID, err)
		}
	}()
	// Yes, it's really "postporocessing" …
	const urlPathSuffix = "recording/postporocessing/done/"

	u := *c.BaseURL
	u.Path = path.Join(u.Path, urlPathSuffix)
	doneURL := u.String()

	// BBB@Scale craps itself when doneURL doesn't end in '/' and we only
	// get a 500 response.
	if !strings.HasSuffix(doneURL, "/") {
		doneURL += "/"
	}

	type request struct {
		BBBMeetingID     string `json:"bbb_meeting_id"`
		BBBRecordingsURL string `json:"bbb_recordings_url"`
		RoomsMeetingID   string `json:"rooms_meeting_id"`
		Token            string `json:"token"`
	}

	body, err := json.Marshal(request{
		// BBB@Scale is a bit connoisseur-y and doesn't enjoy our
		// recording names. Mangle them so they fit.
		BBBMeetingID:     recordingNameReplacer.Replace(recording),
		BBBRecordingsURL: playbackURL,
		RoomsMeetingID:   meetingID,
		Token:            token,
	})
	if err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", doneURL,
		bytes.NewReader(body))
	if err != nil {
		return fmt.Errorf("RegisterServer: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.HTTP.Do(req)
	if err != nil {
		return err
	}
	defer adapters.DrainAndClose(resp.Body)

	if !adapters.StatusOK(resp) {
		return fmt.Errorf("response status %s (code %d)",
			resp.Status, resp.StatusCode)
	}

	return nil
}

// NotifyRecordingDoneWithMetadataHack is like NotifyRecordingDone but pulls
// the meeting ID from recording metadata instead of using the external meeting
// ID received from BBB. This is a temporary workaround for BBB@Scale behaviour
// while awaiting a fix.
func (c *ScaleHTTP) NotifyRecordingDoneWithMetadataHack(
	ctx context.Context,
	recording, meetingID, playbackURL, token string,
	mdBytes []byte,
) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("NotifyRecordingDoneWithMetadataHack: %w", err)
		}
	}()

	var scaleMD struct {
		RoomsMeetingID json.Number `json:"roomsmeetingid"`
	}

	if err := json.Unmarshal(mdBytes, &scaleMD); err != nil {
		return err
	}
	if scaleMD.RoomsMeetingID == "" {
		return errors.New("cannot extract roomsmeetingid from metadata")
	}

	return c.NotifyRecordingDone(ctx, recording, scaleMD.RoomsMeetingID.String(), playbackURL, token)
}

var recordingNameReplacer = strings.NewReplacer("/", "--")

// SyncServerStats transfers server stats to BBB@Scale
func (c *ScaleHTTP) SyncServerStats(ctx context.Context, stats agent.ServerStats) (time.Duration, error) {
	secret, err := c.Secreter.Secret()
	if err != nil {
		return 0, err
	}

	u := *c.BaseURL
	u.Path = path.Join(u.Path, "servers/stats")

	body, err := marshalStats(stats)
	if err != nil {
		return 0, fmt.Errorf("SyncServerStats: %v", err)
	}
	req, err := http.NewRequestWithContext(ctx, "POST", u.String(), bytes.NewReader(body))
	if err != nil {
		return 0, fmt.Errorf("SyncServerStats: %v", err)
	}
	req.Header = setHeader(req.Header, body, c.tenant, secret)

	resp, err := c.HTTP.Do(req)
	if err != nil {
		return 0, fmt.Errorf("SyncServerStats: %w", err)
	}
	defer adapters.DrainAndClose(resp.Body)

	if !adapters.StatusOK(resp) {
		return 0, fmt.Errorf("SyncServerStats: response status %s (code %d)",
			resp.Status, resp.StatusCode)
	}

	var statsResp struct {
		Interval float64 `json:"interval"`
	}
	err = json.NewDecoder(resp.Body).Decode(&statsResp)
	if err != nil {
		return 0, fmt.Errorf("SyncServerStats: unmarshal failed %w", err)
	}

	return time.Duration(statsResp.Interval) * time.Second, nil
}

func marshalStats(stats agent.ServerStats) ([]byte, error) {
	if stats.MeetingStats == nil {
		stats.MeetingStats = make([]*agent.MeetingStats, 0)
	}

	return json.Marshal(stats)
}
