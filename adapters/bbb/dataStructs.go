package bbb

type getMeetingsResponse struct {
	ReturnCode string      `xml:"returncode"`
	Meetings   allMeetings `xml:"meetings"`

	MessageKey string `xml:"messageKey"`
	Message    string `xml:"message"`
}

type allMeetings struct {
	MeetingInfo []meetingInfoResponse `xml:"meeting"`
}

type meetingInfoResponse struct {
	MeetingName       string   `xml:"meetingName"`
	MeetingID         string   `xml:"meetingID"`
	InternalMeetingID string   `xml:"internalMeetingID"`
	AttendeePW        string   `xml:"attendeePW"`
	ModeratorPW       string   `xml:"moderatorPW"`
	ParticipantCount  int      `xml:"participantCount"`
	VideoCount        int      `xml:"videoCount"`
	Metadata          metadata `xml:"metadata"`
	IsBreakout        bool     `xml:"isBreakout"`
}

type metadata struct {
	Origin         string `xml:"bbb-origin"`
	Creator        string `xml:"creator"`
	RoomsMeetingID string `xml:"roomsmeetingid"`
}
