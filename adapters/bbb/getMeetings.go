package bbb

import (
	"context"
	"encoding/xml"
	"fmt"
	"gitlab.com/bbbatscale/bbbatscale-agent/adapters"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"io/ioutil"
	"net/http"
)

// GetMeetingStats requests meeting stats from BBB and parses them
func (c *Client) GetMeetingStats(ctx context.Context) ([]*agent.MeetingStats, error) {

	getMeetings, err := c.getMeetingsCall(ctx)
	if err != nil {
		return nil, err
	}

	return allMeetingStats(getMeetings.Meetings.MeetingInfo), nil
}

func (c *Client) getMeetingsCall(ctx context.Context) (*getMeetingsResponse, error) {
	secret, err := c.Secreter.Secret()
	if err != nil {
		return nil, err
	}

	apiURL := appendChecksum(*c.BaseURL, "getMeetings", nil, secret)

	req, err := http.NewRequestWithContext(ctx, "GET", apiURL.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.HTTP.Do(req)
	if err != nil {
		return nil, err
	}

	all, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = adapters.DrainAndClose(resp.Body)
	if err != nil {
		return nil, err
	}

	var getMeetingsResp getMeetingsResponse
	if err := xml.Unmarshal(all, &getMeetingsResp); err != nil {
		return nil, err
	}

	if getMeetingsResp.ReturnCode != "SUCCESS" {
		return nil, fmt.Errorf("call to BBB failed: %s, %s, %s",
			getMeetingsResp.ReturnCode, getMeetingsResp.MessageKey, getMeetingsResp.Message)
	}

	return &getMeetingsResp, nil
}

func mapMeetingStats(response meetingInfoResponse) *agent.MeetingStats {
	return &agent.MeetingStats{
		MeetingID:        response.MeetingID,
		BBBMeetingID:     response.InternalMeetingID,
		RoomsMeetingID:   response.Metadata.RoomsMeetingID,
		Name:             response.MeetingName,
		IsBreakout:       response.IsBreakout,
		ParticipantCount: response.ParticipantCount,
		VideoCount:       response.VideoCount,
		AttendeePW:       response.AttendeePW,
		ModeratorPW:      response.ModeratorPW,
		Creator:          response.Metadata.Creator,
		Origin:           response.Metadata.Origin,
	}
}

func allMeetingStats(m []meetingInfoResponse) []*agent.MeetingStats {
	var meetings []*agent.MeetingStats
	for _, n := range m {
		meetings = append(meetings, mapMeetingStats(n))
	}
	return meetings
}
