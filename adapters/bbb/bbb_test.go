package bbb

import (
	"encoding/xml"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"net/url"
	"reflect"
	"testing"
)

func TestAppendChecksum(t *testing.T) {
	rawURL := "http://bbb.test/bigbluebutton/api"
	call := "getMeetings"
	secret := "secret"

	cases := []struct {
		params      url.Values
		expectedURL string
	}{
		{
			expectedURL: rawURL + "/" + call + "?checksum=867e6596b930651c0cd4dd1912bec902fae56d5a",
		},
		{
			params:      url.Values{"asdf": []string{"qwer"}},
			expectedURL: rawURL + "/" + call + "?asdf=qwer&checksum=50d1008271acffe177df64d4750badf36d847979",
		},
	}

	for _, c := range cases {
		parsed, err := url.Parse(rawURL)
		if err != nil {
			t.Fatal(err)
		}

		result := appendChecksum(*parsed, call, c.params, []byte(secret))
		actualURLString := result.String()
		if actualURLString != c.expectedURL {
			t.Errorf("expected: %s, actual: %s", c.expectedURL, actualURLString)
		}
	}
}

func TestUnmarshallGetMeetingsResponse(t *testing.T) {
	xmlGetMeetingsResponse := `
<response>
    <returncode>SUCCESS</returncode>
    <meetings>
        <meeting>
            <meetingName>Mcguirefort</meetingName>
            <meetingID>1272df44-aea7-4890-ba86-087122ee1f5e</meetingID>
            <internalMeetingID>65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943</internalMeetingID>
            <createTime>1646301238943</createTime>
            <createDate>Thu Mar 03 09:53:58 UTC 2022</createDate>
            <voiceBridge>21110</voiceBridge>
            <dialNumber>613-555-1234</dialNumber>
            <attendeePW>CWuiF3q9NhwXGCGXRLVmYteaCFYogcax</attendeePW>
            <moderatorPW>tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk</moderatorPW>
            <running>true</running>
            <duration>0</duration>
            <hasUserJoined>true</hasUserJoined>
            <recording>false</recording>
            <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
            <startTime>1646301238947</startTime>
            <endTime>0</endTime>
            <participantCount>1</participantCount>
            <listenerCount>0</listenerCount>
            <voiceParticipantCount>1</voiceParticipantCount>
            <videoCount>0</videoCount>
            <maxUsers>0</maxUsers>
            <moderatorCount>1</moderatorCount>
            <attendees>
                <attendee>
                    <userID>w_gw1lisvaxt7n</userID>
                    <fullName>Sosa, Thomas</fullName>
                    <role>MODERATOR</role>
                    <isPresenter>true</isPresenter>
                    <isListeningOnly>false</isListeningOnly>
                    <hasJoinedVoice>true</hasJoinedVoice>
                    <hasVideo>false</hasVideo>
                    <clientType>HTML5</clientType>
                    <customdata>
                        <bbb_show_participants_on_login>true</bbb_show_participants_on_login>
                        <bbb_record_video>true</bbb_record_video>
                        <bbb_show_public_chat_on_login>true</bbb_show_public_chat_on_login>
                        <bbb_skip_video_preview>false</bbb_skip_video_preview>
                        <bbb_force_restore_presentation_on_new_events>false
                        </bbb_force_restore_presentation_on_new_events>
                        <bbb_skip_video_preview_on_first_join>false</bbb_skip_video_preview_on_first_join>
                        <bbb_listen_only_mode>true</bbb_listen_only_mode>
                        <bbb_auto_join_audio>true</bbb_auto_join_audio>
                        <bbb_skip_check_audio_on_first_join>false</bbb_skip_check_audio_on_first_join>
                        <bbb_auto_swap_layout>false</bbb_auto_swap_layout>
                        <bbb_mirror_own_webcam>false</bbb_mirror_own_webcam>
                        <bbb_auto_share_webcam>false</bbb_auto_share_webcam>
                        <bbb_skip_check_audio>false</bbb_skip_check_audio>
                    </customdata>
                </attendee>
            </attendees>
            <metadata>
                <creator>admin</creator>
                <roomsmeetingid>1272df44-aea7-4890-ba86-087122ee1f5e</roomsmeetingid>
                <streamingurl>None</streamingurl>
                <bbb-origin>BBB@Scale</bbb-origin>
            </metadata>
            <isBreakout>false</isBreakout>
            <breakoutRooms>
                <breakout>320388178a9a44968f1fa4087da0666a3f7a1dff-1646301279931</breakout>
                <breakout>7d301b4d1877d61263a20c04981fc4f6557e8ab5-1646301279926</breakout>
                <breakout>8a3fc52a1b9ab499eb589230abcf4a073fcc3487-1646301279931</breakout>
                <breakout>4299af482c9450301bd781817ee3f3da2462303c-1646301279931</breakout>
                <breakout>b646b683109f7cd1331b0cb7828e2b73a85431c8-1646301279931</breakout>
                <breakout>addf8ba99bfe7fea730371535d107b34770d518e-1646301279931</breakout>
                <breakout>067191439191f92bb6be29f7ba60d989bd2fbe22-1646301279931</breakout>
                <breakout>04cffbce9f4970e47e9a5e809e4289930b1a925b-1646309967428</breakout>
                <breakout>d9fa02e88985f6cd0dae1d1e31ef64e95982d534-1646309967428</breakout>
                <breakout>6df0ef9a1e69a3d3528fa3a9f752b2af92a2f900-1646310888948</breakout>
                <breakout>3f76804b32a84d4fa94256678272c9a30c3c17f7-1646310888948</breakout>
            </breakoutRooms>
        </meeting>
        <meeting>
            <meetingName>Mcguirefort (Raum 2)</meetingName>
            <meetingID>3f76804b32a84d4fa94256678272c9a30c3c17f7-1646310888948</meetingID>
            <internalMeetingID>ccfe869172964096c1cdb4510b1bca3cdc544193-1646310888948</internalMeetingID>
            <createTime>1646310888949</createTime>
            <createDate>Thu Mar 03 12:34:48 UTC 2022</createDate>
            <voiceBridge>211102</voiceBridge>
            <dialNumber>613-555-1234</dialNumber>
            <attendeePW>CWuiF3q9NhwXGCGXRLVmYteaCFYogcax</attendeePW>
            <moderatorPW>tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk</moderatorPW>
            <running>false</running>
            <duration>360</duration>
            <hasUserJoined>false</hasUserJoined>
            <recording>false</recording>
            <hasBeenForciblyEnded>false</hasBeenForciblyEnded>
            <startTime>1646310888957</startTime>
            <endTime>0</endTime>
            <participantCount>0</participantCount>
            <listenerCount>0</listenerCount>
            <voiceParticipantCount>0</voiceParticipantCount>
            <videoCount>0</videoCount>
            <maxUsers>0</maxUsers>
            <moderatorCount>0</moderatorCount>
            <attendees>
            </attendees>
            <metadata>
                <creator>admin</creator>
                <roomsmeetingid>1272df44-aea7-4890-ba86-087122ee1f5e</roomsmeetingid>
                <streamingurl>None</streamingurl>
                <bbb-origin>BBB@Scale</bbb-origin>
            </metadata>
            <isBreakout>true</isBreakout>
            <parentMeetingID>65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943</parentMeetingID>
            <sequence>2</sequence>
            <freeJoin>true</freeJoin>
        </meeting>
    </meetings>
</response>
`

	var getMeetings getMeetingsResponse
	err := xml.Unmarshal([]byte(xmlGetMeetingsResponse), &getMeetings)
	if err != nil {
		t.Fatal(err)
	}

	expected := getMeetingsResponse{
		ReturnCode: "SUCCESS",
		Meetings: allMeetings{
			MeetingInfo: []meetingInfoResponse{
				{
					MeetingName:       "Mcguirefort",
					MeetingID:         "1272df44-aea7-4890-ba86-087122ee1f5e",
					InternalMeetingID: "65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943",
					AttendeePW:        "CWuiF3q9NhwXGCGXRLVmYteaCFYogcax",
					ModeratorPW:       "tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk",
					ParticipantCount:  1,
					VideoCount:        0,
					Metadata: metadata{
						Origin:         "BBB@Scale",
						Creator:        "admin",
						RoomsMeetingID: "1272df44-aea7-4890-ba86-087122ee1f5e",
					},
					IsBreakout: false,
				},
				{
					MeetingName:       "Mcguirefort (Raum 2)",
					MeetingID:         "3f76804b32a84d4fa94256678272c9a30c3c17f7-1646310888948",
					InternalMeetingID: "ccfe869172964096c1cdb4510b1bca3cdc544193-1646310888948",
					AttendeePW:        "CWuiF3q9NhwXGCGXRLVmYteaCFYogcax",
					ModeratorPW:       "tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk",
					ParticipantCount:  0,
					VideoCount:        0,
					Metadata: metadata{
						Origin:         "BBB@Scale",
						Creator:        "admin",
						RoomsMeetingID: "1272df44-aea7-4890-ba86-087122ee1f5e",
					},
					IsBreakout: true,
				},
			},
		},
		MessageKey: "",
		Message:    "",
	}

	if !reflect.DeepEqual(getMeetings, expected) {
		t.Errorf("\nexpected: %+v\nactual: %+v", expected, getMeetings)
	}
}

func TestMapMeetingStats(t *testing.T) {
	resp := meetingInfoResponse{
		MeetingName:       "Mcguirefort",
		MeetingID:         "1272df44-aea7-4890-ba86-087122ee1f5e",
		InternalMeetingID: "65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943",
		AttendeePW:        "CWuiF3q9NhwXGCGXRLVmYteaCFYogcax",
		ModeratorPW:       "tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk",
		ParticipantCount:  1,
		VideoCount:        0,
		Metadata: metadata{
			Origin:         "BBB@Scale",
			Creator:        "admin",
			RoomsMeetingID: "1272df44-aea7-4890-ba86-087122ee1f5e",
		},
		IsBreakout: true,
	}

	actual := mapMeetingStats(resp)

	expected := agent.MeetingStats{
		MeetingID:        "1272df44-aea7-4890-ba86-087122ee1f5e",
		BBBMeetingID:     "65a6c0315e1efc62e5d3cec98422bf77548e0e78-1646301238943",
		RoomsMeetingID:   "1272df44-aea7-4890-ba86-087122ee1f5e",
		Name:             "Mcguirefort",
		IsBreakout:       true,
		ParticipantCount: 1,
		VideoCount:       0,
		AttendeePW:       "CWuiF3q9NhwXGCGXRLVmYteaCFYogcax",
		ModeratorPW:      "tCLJ1rCGAwYVnB0FS9t2RtUMeDG4vZIk",
		Creator:          "admin",
		Origin:           "BBB@Scale",
	}

	if !reflect.DeepEqual(*actual, expected) {
		t.Errorf("\nexpected: %+v \n actual: %+v", expected, actual)
	}
}
