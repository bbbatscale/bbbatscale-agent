package bbb

import (
	"crypto/sha1"
	"encoding/hex"
	"gitlab.com/bbbatscale/bbbatscale-agent/adapters"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"net/url"
	"path"
)

// Client provides interface to BigBlueButton
type Client struct {
	*adapters.Client
}

// New instantiates a new Client
func New(baseURL string, secreter agent.Secreter, option ...adapters.ClientOption) (*Client, error) {

	client, err := adapters.New(baseURL, secreter, option...)
	if err != nil {
		return nil, err
	}

	return &Client{Client: client}, nil
}

func checksum(call string, salt []byte) string {
	toByte := append([]byte(call), salt...)
	checkSumString := sha1.Sum(toByte)

	return hex.EncodeToString(checkSumString[:])
}

func appendChecksum(baseURL url.URL, apiCall string, params url.Values, secret []byte) url.URL {
	if apiCall == "" {
		return baseURL
	}

	if params != nil {
		params["checksum"] = []string{checksum(apiCall+"?"+params.Encode(), secret)}
	} else {
		params = url.Values{
			"checksum": []string{checksum(apiCall, secret)},
		}
	}

	baseURL.Path = path.Join(baseURL.Path, apiCall)
	baseURL.RawQuery = params.Encode()

	return baseURL
}
