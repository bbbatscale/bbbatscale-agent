package adapters

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha512"
	"fmt"
	"gitlab.com/bbbatscale/bbbatscale-agent/agent"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

// A ClientOption may be passed to New to alter the Client's behavior.
type ClientOption func(*Client)

// Client provides an interface to BBBatScale. It is safe for concurrent use
// from multiple goroutines.
type Client struct {
	HTTP     *http.Client
	BaseURL  *url.URL // immutable after initialization
	Secreter agent.Secreter
}

// WithHTTPClient returns a ClientOption that makes the Client use the provided
// http.Client (instead of http.DefaultClient).
func WithHTTPClient(cli *http.Client) ClientOption {
	return func(c *Client) {
		c.HTTP = cli
	}
}

// New instantiates a client to BBB@Scale
func New(theURL string, secreter agent.Secreter, opts ...ClientOption) (*Client, error) {
	u, err := url.Parse(theURL)
	if err != nil {
		return nil, fmt.Errorf("bbbatscale.New: %w", err)
	}

	c := &Client{
		HTTP:     http.DefaultClient,
		BaseURL:  u,
		Secreter: secreter,
	}
	for _, opt := range opts {
		opt(c)
	}

	return c, nil
}

// StatusOK checks for http status 2XX
func StatusOK(r *http.Response) bool {
	return 200 <= r.StatusCode && r.StatusCode < 300
}

// DrainAndClose drains and closes ReadCloser
func DrainAndClose(r io.ReadCloser) error {
	io.Copy(ioutil.Discard, r)
	return r.Close()
}

// ComputeMACHeader calculates a HMAC over given arguments
func ComputeMACHeader(key []byte, t time.Time, body []byte) string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "%d.%s", t.Unix(), body)

	mac := hmac.New(sha512.New, key)
	mac.Write(b.Bytes())

	return fmt.Sprintf("t=%d,v1=%x", t.Unix(), mac.Sum(nil))
}
