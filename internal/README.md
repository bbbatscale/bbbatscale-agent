# Internal packages from gRPC

The gRPC project is not yet ready to support these packages as stable
interfaces.  Hence, the way to use this code for the moment is to copy it and
maintain it just as if we had written it ourselves.

## Licensing

For code in this directory, the following conditions apply:

```
Copyright 2017 gRPC authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
